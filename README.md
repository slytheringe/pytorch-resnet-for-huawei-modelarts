# pytorch-resnet-for-huawei-modelarts

## 介绍
- 本工程为一个可在华为ModelArts云服务上运行与推理的示例程序，该工程使用**pytorch1.4**实现。  
- 工程使用resnet对垃圾进行分类，可选择torchvision中的resnet18一直到resnet152作为基础网络。  
- 建议使用在imagenet上预训练好的模型基础上进行训练。推理使用的代码在deploy_scripts文件夹中使用时需把data与models文件夹拷贝进来。  
#### 预训练模型下载地址  
[resnet18](https://download.pytorch.org/models/resnet18-5c106cde.pth) | [resnet34](https://download.pytorch.org/models/resnet34-333f7ec4.pth) | [resnet50](https://download.pytorch.org/models/resnet50-19c8e357.pth) | [resnet101](https://download.pytorch.org/models/resnet101-5d3b4d8f.pth) | [resnet152](https://download.pytorch.org/models/resnet152-b121ed2d.pth)  
### 文件组织形式
|root| 说明 |  
|:--|--|
|./data |  存放自定义的pytorch数据集生成程序、自定义的数据扩增与图像预处理|
|./deploy_scripts|在ModelArts部署在线服务或者参加比赛必备得线上推理代码|
|./models|存放构建模型相关代码|
|train.py|模型训练代码，可直接在本地运行用于测试|
|run.py|使用ModelArts进行线上训练脚本。用于对接云端api，配置训练时超参数|

### 可配置的超参数说明
传入参数名称     | 作用
-------- | :-----
train_url | 
data_url    | 
local_root    | 
pretrained_path | 保存在obs上的预训练模型链接，可以是在imagenet上预训练好的模型也可以是先前训练的模型进行二次训练
resnet_type| 使用哪种resnet。支持resnet18、resnet34、resnet50、resnet101、resnet152
num_classes| 分类的个数，在华为垃圾分类比赛中为43类
train_ratio| 分割数据集比例。例如希望数据集中0.9的数据用来训练0.1用来验证，则填0.9
batch_size|
num_workers|
lr| 开始训练时的学习率
momentum|
epoch|
steps| 当使用multisteps时需要填这个超参。如1,3,5指在训练的第1、3、5个epoch时将学习率调整为原来的gamma倍
lr_schedule| 学习率调整方法支持 cosine、multisteps、exponential
gamma| 当选择multisteps、exponential时需要填这个超参