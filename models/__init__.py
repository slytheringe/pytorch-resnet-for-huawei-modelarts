from .my_model import create_resnetX_from_pretrained
from .my_model import MyResNet

__all__ = ['create_resnetX_from_pretrained', 'MyResNet']